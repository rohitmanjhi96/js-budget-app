//Budget controller
var budgetController = (function() {
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = -1;
    };
    Expense.prototype.calcPercentage = function (totalIncome) {
    if (totalIncome > 0) {
        this.percentage = Math.round((this.value / totalIncome) * 100)
    } else {
        this.percentage = -1;
    }
    };

    Expense.prototype.getPercentage = function () {
        return this.percentage;
    }

    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var calculateTotal = function (type) {
        var sum = 0;
        data.allItems[type].forEach(function (current, index, value) {
            sum = sum + current.value;
        });
        data.totals[type] = sum;
    };

    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc:0
        },
        budget: 0,
        percentage: -1,

    }
    return {
        addItem: function (type, des, val) {
            var newItem;
            // Create new Id
            if (data.allItems[type].length > 0) {
                var ID = data.allItems[type][data.allItems[type].length - 1].id + 1;;
                // alert(ID)
            } else {
                ID = 0;
            }
            // create new iten based on 'inc' or 'exp' type
            if (type === 'exp') {
                newItem = new Expense(ID, des, val);
            }
            else if (type === 'inc') {
                newItem = new Income(ID, des, val)
            }
            // push into our datastructure
            data.allItems[type].push(newItem);
            // Return the new element
            return newItem;

        },
        data: function () {
            return data;
        },
        deleteItem: function (type, id) {
            var ids, index;
            ids = data.allItems[type].map(function (current) {
                return current.id
            });
            index = ids.indexOf(id);

            if (index !== -1) {
                data.allItems[type].splice(index, 1)
            }
        },

        CalculateBudget: function () {
            // calculate total invoke and expenses
            calculateTotal('inc');
            calculateTotal('exp');
            // calculate th ebudget: income-expenses
            data.budget = data.totals.inc - data.totals.exp;
            // calculate the percentage of income that we spent
            if (data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100)
                console.log(data.percentage)
            } else {
                data.percentage = -1
            }
        },

        calculatePercentages: function () {
            data.allItems.exp.forEach(function (curr) {
                curr.calcPercentage(data.totals.inc)
            })
        },

        getPercentages: function () {
            var allPerc = data.allItems.exp.map(function (curr) {
                return curr.getPercentage();
            })
            return allPerc
        },
        getBudget: function () {
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            };
        },
        testing: function () {
            console.log(data)
        }
    }
})();

//UI Controller
var UIController = (function () {
    var DOMstrings = {
        inputType: ".add__type",
        inputDescription: ".add__description",
        inputValue: ".add__value",
        inputBtn: ".add__btn",
        incomesContainer: ".income__list",
        expensesContainer: ".expenses__list",
        budgetlabel: ".budget__value",
        incomeLable: ".budget__income--value",
        expenseLabel: ".budget__expenses--value",
        percentageLable: ".budget__expenses--percentage",
        container: ".container",
        expensesPercLabel: ".item__percentage",
        dateLabel: '.budget__title--month'
    };

    var formatNumber = function (num, type) {
        var numSplit, int, dec, type;
        num = Math.abs(num);
        num = num.toFixed(2);
        numSplit = num.split('.');

        int = numSplit[0];
        if (int.length > 3) {
            int = int.substr(0, int.length - 3) + ',' + int.substr(int.length - 3, int.length);
            // alert(int);
        }
        dec = numSplit[1];
        // alert(dec)
        return (type === 'exp' ? '-' : '+') + ' ' + int + '.' + dec;

    };
    var nodeListForEach = function (list, callback) {
        for (var i = 0; i < list.length; i++) {
            callback(list[i], i);
        }
    };

    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMstrings.inputType).value,
                //will be either inc or exp
                description: document.querySelector(DOMstrings.inputDescription)
                    .value,
                value: parseFloat(document.querySelector(DOMstrings.inputValue).value)
            };
        },

        addListItem: function (obj, type) {
            var html, newHtml, element;
            // create html string with placeholder text
            if (type === 'inc') {
                element = DOMstrings.incomesContainer;
                html = `<div class="item clearfix" id="inc-${obj.id}"><div class="item__description">${obj.description}</div><div class="right clearfix"><div class="item__value">${formatNumber(obj.value, type)}</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button> </div></div></div>`
            } else if (type === 'exp') {
                element = DOMstrings.expensesContainer;
                html = `<div class="item clearfix" id="exp-${obj.id}"><div class="item__description">${obj.description}</div><div class="right clearfix"><div class="item__value">${formatNumber(obj.value, type)}</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button> </div></div></div>`
            }


            //Insert the HTML into the DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', html)

        },

        deleteListItem: function (selectorID) {
            var el = document.getElementById(selectorID);
            el.parentNode.removeChild(el)
        },
        clearFields: function () {
            var fields;
            fields = document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);

            var fieldsArr = Array.prototype.slice.call(fields);

            fieldsArr.forEach(function (current, index, array) {
                current.value = "";

            });
            fieldsArr[0].focus();
        },

        displayBudget: function (obj) {
            var type;
            obj.budget > 0 ? type = 'inc' : type = 'exp';
            document.querySelector(DOMstrings.budgetlabel).textContent = formatNumber(obj.budget, type);
            document.querySelector(DOMstrings.incomeLable).textContent = formatNumber(obj.totalInc, 'inc');
            document.querySelector(DOMstrings.expenseLabel).textContent = formatNumber(obj.totalExp, 'exp');
            if (obj.percentage > 0) {
                document.querySelector(DOMstrings.percentageLable).textContent = obj.percentage + '%';
            } else {
                document.querySelector(DOMstrings.percentageLable).textContent = '---';
            }
        },
        displayPercentages: function (percentages) {
            console.log(percentages[0] + '------------------')
            var fields = document.querySelectorAll(DOMstrings.expensesPercLabel);


            nodeListForEach(fields, function (current, index) {
                // alert(percentages[index])
                if (percentages[index] > 0) {
                    current.textContent = percentages[index] + '%';
                } else {
                    current.textContent = '-----';
                }
            })
        },

        displayMonth: function () {
            var now, year, month;
            months = ['january', 'february', 'march', 'April', 'May', 'June', 'July', 'August', 'Sept', 'Oct', 'Nov', 'Dec']
            now = new Date();
            year = now.getFullYear();
            month = now.getMonth();
            alert(year)
            document.querySelector(DOMstrings.dateLabel).textContent = months[month] + ' ' + year;

        },

        changedType: function () {

            var fields = document.querySelectorAll(
                DOMstrings.inputType + ',' +
                DOMstrings.inputDescription + ',' +
                DOMstrings.inputValue);

            nodeListForEach(fields, function (curr) {
                 curr.classList.toggle('red-focus');
            });

            document.querySelector(DOMstrings.inputBtn).classList.toggle('red')
        },


        getDOMstrings: function() {
            return DOMstrings;
        }
    };
})();

//Global App Controller
var controller = (function (budgetctrl, uictrl) {
    var setupEventListners = function () {
        var DOM = uictrl.getDOMstrings();
        document.querySelector(DOM.inputBtn).addEventListener("click", ctrlAddItem);

        document.addEventListener("keypress", function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                 ctrlAddItem();
        }
        });

        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem)

        document.querySelector(DOM.inputType).addEventListener('change', uictrl.changedType )
    }


    var updateBudget = function () {
        // 1. Calculate the budget
        budgetctrl.CalculateBudget();
        // 2. Return the Budget
        var budget = budgetctrl.getBudget();
        // 3. Display the budget on the UI
        uictrl.displayBudget(budget);
        console.log(budget)

    };

    var updatePercentages = function () {
        //1. calculate percentage
        budgetctrl.calculatePercentages();
        //2. Read percentage of budget controller
        var percentage = budgetctrl.getPercentages()
        //3. Update the UI with th e new percentage
        console.log(percentage)
        uictrl.displayPercentages(percentage)
    }
    var ctrlAddItem = function () {
        // 1. Get the field input data
        var input = uictrl.getInput();
        console.log(input);
        if (input.description !== "" && !isNaN(input.value) && input.value >0) {
            // 2. Add the item to the budget contriller
        var newItem = budgetctrl.addItem(input.type, input.description, input.value)
        // 3. Add the item to the UI
        UIController.addListItem(newItem, input.type);
        //4. Clear the fields
        uictrl.clearFields();
        // 5. Calculate and update Budget
            updateBudget();
        //6. Calculate and update percentage
            updatePercentages()
        }

    };

    var ctrlDeleteItem = function (event) {
        var itemID, ID, splitID, type;
        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id

        if (itemID) {
            // inc-1
            splitID = itemID.split('-');
            type = splitID[0];
            ID = parseInt(splitID[1]);

            // 1. delete thr item from the data structure
            budgetctrl.deleteItem(type, ID);
            //2. delete from the ui
            uictrl.deleteListItem(itemID)
            //3.Update and show the new budget
            updateBudget();
            //4. Calculate and update percentage
            updatePercentages()
        }
    };

    return {
        init: function () {
            console.log('Application has started')
            uictrl.displayMonth();
            uictrl.displayBudget({
                budget: 0,
                totalInc: 0,
                totalExp: 0,
                percentage: 0
            });

            setupEventListners();
        }
    }
})(budgetController, UIController);

controller.init()
