budgetController = (function() {})();

var UIController = (function() {
    var DomString = {
        inputTypes: ".add__type",
        inputDescription: ".add__description", // 3. Spelling mistake
        inputValues: ".add__value",
        inputBtn: ".add__btn" // 1. Error one . is missing
    };
    return {
        getInput: function() {
            return {
                type: document.querySelector(DomString.inputTypes).value,
                discription: document.querySelector(DomString.inputDescription)
                    .value,
                value: document.querySelector(DomString.inputValues).value //2. "." after value
            };
        },
        getDomString: function() {
            return DomString;
        }
    };
})();

var controller = (function(budgetCtrl, UICtrl) {
    var Dom = UICtrl.getDomString();
    var ctrlAddItem = function() {
        // get the field input data.
        var input = UICtrl.getInput();
        console.log(input);
        // add the item to the budget controller

        //add the item to the ui

        //calculate the budget

        // display the budget on the ui
        console.log("its work");
    };

    document.querySelector(Dom.inputBtn).addEventListener("click", ctrlAddItem);
    document.addEventListener("keypress", function (event) {
        console.log(event)
        if (event.keyCode === 13 || event.which === 13) {
            ctrlAddItem();
        }
    });
})(budgetController, UIController);
